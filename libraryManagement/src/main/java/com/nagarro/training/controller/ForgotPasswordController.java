package com.nagarro.training.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.nagarro.training.constants.GlobalConstants;
import com.nagarro.training.model.UserModel;
import com.nagarro.training.service.AuthenticationServiceInterface;

@Controller
public class ForgotPasswordController {
	@Autowired
	private AuthenticationServiceInterface authenticationService;

	@ModelAttribute(GlobalConstants.USER_MODEL_ATTRIBUTE)
	public UserModel formBackingObject() {
		return new UserModel();
	}

	@GetMapping(value = GlobalConstants.FORGOT_PASSWORD)
	public String init() {
		return GlobalConstants.FORGOT_PASSWORD + GlobalConstants.JSP;
	}

	@PostMapping(GlobalConstants.FORGOT_PASSWORD)
	public String submit(@ModelAttribute(GlobalConstants.USER_MODEL_ATTRIBUTE) UserModel userModel, ModelMap model,
			HttpSession session) {
		if (authenticationService.updatePassword(userModel.getUserName(), userModel.getPassword())) {
			return GlobalConstants.REDIRECT + GlobalConstants.LOGIN;
		} else {
			model.addAttribute(GlobalConstants.FORGOT_PASSWORD_ERROR, GlobalConstants.NO_USER);
			return GlobalConstants.FORGOT_PASSWORD + GlobalConstants.JSP;
		}
	}
}
