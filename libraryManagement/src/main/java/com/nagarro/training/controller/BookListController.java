package com.nagarro.training.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.RestTemplate;

import com.nagarro.training.constants.GlobalConstants;
import com.nagarro.training.model.BookModel;
import com.nagarro.training.service.impl.BookServiceImplementation;

@Controller
public class BookListController {
	@Autowired
	RestTemplate restTemplate;
	@Autowired
	BookServiceImplementation bookService;

	@GetMapping(value = "/" + GlobalConstants.BOOKS)
	public String getBookList(Model model) {
		List<BookModel> bookModelList = bookService.getAllBooks();
		model.addAttribute(GlobalConstants.BOOK_LIST, bookModelList);
		return GlobalConstants.BOOKS + GlobalConstants.JSP;
	}
}
