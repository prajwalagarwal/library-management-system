package com.nagarro.training.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.RestTemplate;

import com.nagarro.training.constants.GlobalConstants;
import com.nagarro.training.service.impl.BookServiceImplementation;

@Controller
public class DeleteBookController {
	@Autowired
	RestTemplate restTemplate;
	@Autowired
	BookServiceImplementation bookService;

	@GetMapping(value = "/"+GlobalConstants.DELETE)
	String deleteBook(HttpServletRequest request) {
		int bookCode = Integer.parseInt(request.getParameter("id"));
		bookService.deleteBook(bookCode);
		return GlobalConstants.REDIRECT+GlobalConstants.BOOKS;
	}

}
