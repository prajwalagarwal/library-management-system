package com.nagarro.training.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.nagarro.training.constants.GlobalConstants;
import com.nagarro.training.model.AuthorModel;
import com.nagarro.training.model.BookModel;
import com.nagarro.training.service.impl.BookServiceImplementation;

@Controller
public class AddBookDetailController {
	@Autowired
	RestTemplate restTemplate;
	@Autowired
	BookServiceImplementation bookService;

	@ModelAttribute(GlobalConstants.BOOK_MODEL)
	public BookModel formBackingObject() {
		BookModel bookModel = new BookModel();
		bookModel.setDate(new Date());
		return bookModel;
	}

	@GetMapping(value = "/" + GlobalConstants.ADD_BOOK_REQUEST)
	public String initAddBookPage(Model model, HttpSession session) {
		List<AuthorModel> authorModelList = bookService.getAllAuthors();
		session.setAttribute(GlobalConstants.AUTHOR_LIST, authorModelList);
		return GlobalConstants.ADD_BOOK_REQUEST + GlobalConstants.JSP;
	}

	@PostMapping(value = "/" + GlobalConstants.ADD_BOOK_REQUEST)
	public String addBook(@ModelAttribute(GlobalConstants.BOOK_MODEL) BookModel bookModel, BindingResult bindingResult,
			Model model) {
		try {
			bookService.addBook(bookModel);
			model.addAttribute(GlobalConstants.MESSAGE, GlobalConstants.ADD_SUCCESS);
			return GlobalConstants.REDIRECT + GlobalConstants.BOOKS;
		} catch (HttpStatusCodeException ex) {
			if (ex.getRawStatusCode() == 500) {
				model.addAttribute(GlobalConstants.MESSAGE, GlobalConstants.BOOK_ALREADY_PRESENT);
			} else {
				model.addAttribute("message", "Something went wrong");
			}
			return GlobalConstants.ADD_BOOK_REQUEST + GlobalConstants.JSP;
		}
	}
}
