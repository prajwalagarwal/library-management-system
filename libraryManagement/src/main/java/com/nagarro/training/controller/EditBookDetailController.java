package com.nagarro.training.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.nagarro.training.constants.GlobalConstants;
import com.nagarro.training.model.AuthorModel;
import com.nagarro.training.model.BookModel;
import com.nagarro.training.service.impl.BookServiceImplementation;

@Controller
public class EditBookDetailController {
	@Autowired
	RestTemplate restTemplate;
	@Autowired
	BookServiceImplementation bookService;

	@ModelAttribute(GlobalConstants.BOOK_MODEL)
	public BookModel formBackingObject(HttpServletRequest request, HttpSession session) {
		int bookCode;
		if (request.getParameter("id") != null) {
			bookCode = Integer.parseInt(request.getParameter("id"));
			session.setAttribute(GlobalConstants.BOOK_CODE, bookCode);
		} else {
			bookCode = (int) session.getAttribute(GlobalConstants.BOOK_CODE);
		}
		BookModel bookModel = bookService.getBookDetail(bookCode);

		return bookModel;
	}

	@GetMapping(value = "/" + GlobalConstants.EDIT_BOOK)
	public String initEditBookPage(HttpServletRequest request, HttpSession session) {
		List<AuthorModel> authorModelList = bookService.getAllAuthors();
		session.setAttribute(GlobalConstants.AUTHOR_LIST, authorModelList);
		return GlobalConstants.EDIT_BOOK + GlobalConstants.JSP;
	}

	@PostMapping(value = "/" + GlobalConstants.EDIT_BOOK)
	public String updateBook(@ModelAttribute(GlobalConstants.BOOK_MODEL) BookModel bookModel,
			BindingResult bindingResult, Model model) {
		try {
			bookService.updateBook(bookModel);
			model.addAttribute(GlobalConstants.MESSAGE, GlobalConstants.BOOK_UPDATED);
		} catch (HttpStatusCodeException ex) {
			model.addAttribute(GlobalConstants.MESSAGE, GlobalConstants.ERROR_MESSAGE_TEXT);
			return GlobalConstants.EDIT_BOOK + GlobalConstants.JSP;
		}
		return GlobalConstants.REDIRECT + GlobalConstants.BOOKS;
	}
}
