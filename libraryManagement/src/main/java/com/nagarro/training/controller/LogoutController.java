package com.nagarro.training.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import com.nagarro.training.constants.GlobalConstants;

@Controller
public class LogoutController {
	@GetMapping(value = GlobalConstants.LOGOUT)
	public String init(HttpSession session) {
		session.setAttribute(GlobalConstants.USERNAME, null);
		return GlobalConstants.REDIRECT + GlobalConstants.LOGIN;
	}
}
