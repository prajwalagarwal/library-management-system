package com.nagarro.training.constants;

public class GlobalConstants {

	public static final String REDIRECT = "redirect:/";
	public static final String BOOK_MODEL = "bookModel";
	public static final String AUTHOR_LIST = "authorList";
	public static final String ADD_BOOK_REQUEST = "add-book";
	public static final String BOOKS = "books";
	public static final Object BOOK_ALREADY_PRESENT = "Book already present with same book code";
	public static final String BOOK_LIST = "bookList";
	public static final String MESSAGE = "message";
	public static final Object ADD_SUCCESS = "Book added successfully";
	public static final String JSP = ".jsp";
	public static final String DELETE = "delete";
	public static final String BOOK_CODE = "bookCode";
	public static final Object BOOK_UPDATED = "Book updated successfully";
	public static final String ERROR_MESSAGE = "errorMessage";
	public static final String ERROR_MESSAGE_TEXT = "Something went wrong";
	public static final String EDIT_BOOK = "edit-book";

	public static final String INVALID_USER = "Invalid user or password";
	public static final String NO_USER = "User Does Not Exist";

	public static final String USERNAME = "username";
	public static final String PASSWORD = "password";
	public static final String FORGOT_PASSWORD_ERROR = "forgotPasswordError";

	public static final String USER_MODEL_ATTRIBUTE = "userModel";
	public static final String FORGOT_PASSWORD = "forgot-password";

	public static final String LOGIN = "login";

	public static final String LOGOUT = "logout";
	public static final String ENTITY_PACKAGE = "com.nagarro.training.entity";
	public static final String BASE_PACKAGE = "com.nagarro.training";
	
	public static final String BASE_URL="http://localhost:8099/librarymanagement/v1/";
	public static final String BOOK_END_POINT="books";
	public static final String AUTHOR_END_POINT="authors";

}
