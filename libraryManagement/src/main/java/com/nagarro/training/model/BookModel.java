package com.nagarro.training.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

@Component
public class BookModel {

	private int bookCode;
	private String bookName;
	private AuthorModel author;
	@DateTimeFormat(pattern = "d MMMM yyyy")
	private Date date;

	public BookModel() {
		super();
	}

	public BookModel(int bookCode, String bookName, AuthorModel author, Date date) {
		super();
		this.bookCode = bookCode;
		this.bookName = bookName;
		this.author = author;
		this.date = date;
	}

	public int getBookCode() {
		return bookCode;
	}

	public void setBookCode(int bookCode) {
		this.bookCode = bookCode;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public AuthorModel getAuthor() {
		return author;
	}

	public void setAuthor(AuthorModel author) {
		this.author = author;
	}

	public Date getDate() {
//		SimpleDateFormat formatter = new SimpleDateFormat("d MMMM yyyy");
//		String strDate= formatter.format(date);
		return date;
	}

	public String getDateInformat() {
		SimpleDateFormat formatter = new SimpleDateFormat("d MMMM yyyy");
		String strDate = formatter.format(date);
		return strDate;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BookModel [bookCode=" + bookCode + ", bookName=" + bookName + ", author=" + author + ", date=" + date
				+ "]";
	}

}
