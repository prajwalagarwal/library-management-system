package com.nagarro.training.model;

import org.springframework.stereotype.Component;

@Component
public class AuthorModel {

	private int authorId;
	private String name;

	public AuthorModel() {
		super();
	}

	public AuthorModel(int authorId, String name) {
		super();
		this.authorId = authorId;
		this.name = name;
	}

	public int getAuthorId() {
		return authorId;
	}

	public void setAuthorId(int authorId) {
		this.authorId = authorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
