package com.nagarro.training.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.nagarro.training.entity.UserEntity;
import com.nagarro.training.repository.LoginDaoInterface;

@Repository
public class LoginDaoImplementation implements LoginDaoInterface {

	private final String CHECK_USER_QUERY = "FROM UserEntity U WHERE U.userName = :userName";
	@Autowired
	public HibernateTemplate hibernateTemplate;

	@Transactional
	@Override
	public boolean validate(String userName, String password) {
		System.out.println(userName + " " + password);
		UserEntity user = (UserEntity) hibernateTemplate.getSessionFactory().getCurrentSession()
				.createQuery(CHECK_USER_QUERY).setParameter("userName", userName).uniqueResult();
		if (user != null && user.getPassword().equals(password)) {
			// commit transaction
			return true;
		}
		return false;
	}

	@Transactional
	@Override
	public boolean updatePassword(String userName, String password) {

		UserEntity user = null;
		user = (UserEntity) hibernateTemplate.getSessionFactory().getCurrentSession().createQuery(CHECK_USER_QUERY)
				.setParameter("userName", userName).uniqueResult();
		if (user != null) {
			user.setPassword(password);
			hibernateTemplate.update(user);
			return true;
		}
		return false;
	}

	@Transactional
	@Override
	public int getUserId(String userName) {
		UserEntity user = (UserEntity) hibernateTemplate.getSessionFactory().getCurrentSession()
				.createQuery(CHECK_USER_QUERY).setParameter("userName", userName).uniqueResult();
		return user.getId();
	}
}
