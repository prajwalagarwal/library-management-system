package com.nagarro.training.repository;

public interface LoginDaoInterface {
	boolean validate(String userName, String password);

	boolean updatePassword(String userName, String password);

	public int getUserId(String userName);

}
