package com.nagarro.training.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nagarro.training.repository.LoginDaoInterface;
import com.nagarro.training.service.AuthenticationServiceInterface;

@Service
public class AuthenticationServiceImplementation implements AuthenticationServiceInterface {
	@Autowired
	private LoginDaoInterface loginDao;

	@Override
	public boolean validate(String username, String password) {
		// TODO Auto-generated method stub
		return this.loginDao.validate(username, password);
	}

	@Override
	public int getUserId(String username) {
		return this.loginDao.getUserId(username);
	}

	@Override
	public boolean updatePassword(String username, String password) {
		// TODO Auto-generated method stub
		return this.loginDao.updatePassword(username, password);
	}

}
