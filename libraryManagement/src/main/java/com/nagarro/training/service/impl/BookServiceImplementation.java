package com.nagarro.training.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nagarro.training.constants.GlobalConstants;
import com.nagarro.training.model.AuthorModel;
import com.nagarro.training.model.BookModel;
import com.nagarro.training.service.BookServiceInterface;

@Service
public class BookServiceImplementation implements BookServiceInterface {
	@Autowired
	RestTemplate restTemplate;

	@Override
	public List<BookModel> getAllBooks() {
		ResponseEntity<String> response = restTemplate
				.getForEntity(GlobalConstants.BASE_URL + GlobalConstants.BOOK_END_POINT, String.class);
		List<BookModel> bookModelList = null;
		try {
			JSONObject jsonObject = (JSONObject) JSONValue.parse(response.getBody());
			JSONArray jsonArray = (JSONArray) jsonObject.get("data");
			ObjectMapper mapper = new ObjectMapper();
			bookModelList = mapper.readValue(jsonArray.toJSONString(), new TypeReference<List<BookModel>>() {
			});
		} catch (JsonMappingException e) {
		} catch (JsonProcessingException e) {
		} catch (Exception e) {
		}
		return bookModelList;
	}

	@Override
	public List<AuthorModel> getAllAuthors() {
		ResponseEntity<String> authorsResponse = restTemplate
				.getForEntity(GlobalConstants.BASE_URL + GlobalConstants.AUTHOR_END_POINT, String.class);
		JSONObject jsonObject = (JSONObject) JSONValue.parse(authorsResponse.getBody());
		JSONArray jsonArray = (JSONArray) jsonObject.get("data");
		ObjectMapper mapper = new ObjectMapper();
		List<AuthorModel> authorModelList = null;
		try {
			authorModelList = mapper.readValue(jsonArray.toJSONString(), new TypeReference<List<AuthorModel>>() {
			});
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return authorModelList;
	}

	@Override
	public void deleteBook(int bookCode) {
		restTemplate.delete(GlobalConstants.BASE_URL + GlobalConstants.BOOK_END_POINT + "/" + bookCode);
	}

	@Override
	public void addBook(BookModel bookModel) throws HttpStatusCodeException {
		ResponseEntity<String> response = restTemplate
				.postForEntity(GlobalConstants.BASE_URL + GlobalConstants.BOOK_END_POINT, bookModel, String.class);

	}

	@Override
	public BookModel getBookDetail(int bookCode) {
		ResponseEntity<String> authorsResponse = restTemplate
				.getForEntity(GlobalConstants.BASE_URL + GlobalConstants.BOOK_END_POINT + "/" + bookCode, String.class);
		JSONObject jsonObject = (JSONObject) JSONValue.parse(authorsResponse.getBody());
		JSONObject jsonArray = (JSONObject) jsonObject.get("data");
		ObjectMapper mapper = new ObjectMapper();
		BookModel bookModel = new BookModel();
		try {
			bookModel = mapper.readValue(jsonArray.toJSONString(), new TypeReference<BookModel>() {
			});
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return bookModel;

	}

	@Override
	public void updateBook(BookModel bookModel) throws HttpStatusCodeException {
		Map<String, Integer> params = new HashMap<String, Integer>();
		params.put("id", bookModel.getBookCode());
		HttpEntity<BookModel> entity = new HttpEntity<BookModel>(bookModel);
		ResponseEntity<String> response = restTemplate.exchange(
				GlobalConstants.BASE_URL + GlobalConstants.BOOK_END_POINT + "/{id}", HttpMethod.PUT, entity,
				String.class, bookModel.getBookCode());
	}

}
