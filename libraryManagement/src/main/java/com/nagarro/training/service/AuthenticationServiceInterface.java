package com.nagarro.training.service;

public interface AuthenticationServiceInterface {
	boolean validate(String username, String password);

	public int getUserId(String username);

	boolean updatePassword(String username, String password);

}
