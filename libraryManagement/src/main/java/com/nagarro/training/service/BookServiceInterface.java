package com.nagarro.training.service;

import java.util.List;

import org.springframework.web.client.HttpStatusCodeException;

import com.nagarro.training.model.AuthorModel;
import com.nagarro.training.model.BookModel;

public interface BookServiceInterface {

	List<BookModel> getAllBooks();

	List<AuthorModel> getAllAuthors();

	void deleteBook(int bookCode);

	void addBook(BookModel bookModel) throws HttpStatusCodeException;

	BookModel getBookDetail(int bookCode);

	void updateBook(BookModel bookModel);

}
