<%@ taglib prefix="c" 
       uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="fn" 
       uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page isELIgnored="false" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>

<head>
    <title>Admin Login</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<body class="vh-100">
    <div class="p-2 fs-1 text-center" style="background-color: black;color: white;">Welcome to Library Management Tool
    </div>
    <div class="d-flex flex-column container-fluid align-items-center justify-content-center h-75">

        <form:form action="login" method="post" modelAttribute="userModel" class="w-100">
            <div class="col container-fluid px-4">
                <div class="row border fw-bold fs-6 py-2 px-1" style="background-color: #E1F6FF;color: #004764">
                    Login
                </div>
                <div class="row border">
                    <table class="table table-borderless">
                        <tbody>
                            <tr>
                                <td>
                                    Username<span style="color: red">*</span>
                                </td>
                                <td>
                                    <form:input path="userName" required="required" minlength="5" maxlength="50"/>
                                </td>
                            </tr>
                            <tr class="table table-borderless">
                                <td>
                                    Password<span style="color: red">*</span>
                                </td>
                                <td>
                                    <form:input path="password" type="password" required="required" minlength="5" maxlength="50"/>
                                </td>
                            </tr>
                            <tr>
                                <td>

                                </td>
                                <td>
                                    <a href="forgot-password">Forgot Password</a>
                                </td>
                            </tr>
                            <tr>
                                <td>

                                </td>
                                <td style="color: red">
                                    ${errorMessage}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="row border p-2 justify-content-end" style="background-color: #E1F6FF;color: #004764">
                    <button type="submit" class="btn btn-primary btn-sm w-auto">Login</button>
                </div>
            </div>
        </form:form>
    </div>
</body>

</html>