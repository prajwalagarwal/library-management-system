<%@ taglib prefix="c" 
       uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="fn" 
       uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page isELIgnored="false" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add Book</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body class="vh-100">
<% 
     
     response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");
     if(session.getAttribute("username")==null){ 
    	 response.sendRedirect("login"); 
   	} 
     
     
     %>
<div style="background-color: black;color: white;">
        <div class="p-2 fs-3 text-center" style="position: relative;">

            Welcome to Library Management Tool</div>
        <div class="d-flex flex-row align-items-center" style="position: absolute;top: 10px;right: 0px;">

            <button type="button" onClick="window.location='logout'" value="Logout"
                class="btn ml-2 btn btn-secondary mx-2">
                Logout
            </button>
        </div>
    </div>
    <div class="container d-flex flex-column h-75 justify-content-center align-items-center">
        <a href="books"><span class="material-icons">arrow_back</span></a>
        <form:form action="add-book" method="post" modelAttribute="bookModel">
            <table class="table w-auto m-auto table-bordered">
                <thead>
                    <tr>
                        <th colspan="2">Add Book Detail</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                            <td>
                                Book Code<span style="color: red">*</span>
                            </td>
                            <td>
                           		 <form:input type="number" path="bookCode" required="required" placeholder="Enter Book Code" value=""/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Book Name<span style="color: red">*</span>
                            </td>
                            <td>
                             	<form:input path="bookName" required="required" placeholder="Enter Book Name"  />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Author<span style="color: red">*</span>
                            </td>
                            <td>
                            <form:select path="author.authorId">
								    <form:options items="${authorList}" itemValue="authorId" itemLabel="name" />
								</form:select>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                Added On
                            </td>
                            <td>
                            	<c:out value="${bookModel.getDateInformat()}" />
                            </td>
                        </tr>
                        
                        <tr>
                            <td>

                            </td>
                            <td class="d-flex flex-column">
                                <input type="submit">
                                <span style="color:red;"> ${message}</span>
                            </td>
                        </tr>
                </tbody>
            </table>
        </form:form>
    </div>
</body>
</html>