<%@ taglib prefix="c" 
       uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="fn" 
       uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page isELIgnored="false" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Library Management</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
<% 
     
     response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");
     if(session.getAttribute("username")==null){ 
    	 response.sendRedirect("login"); 
   	} 
     
     
     %>
     <div style="background-color: black;color: white;">
            <div class="p-2 fs-3 text-center" style="position: relative;">

                Welcome to Library Management Tool
           </div>
            <div class="d-flex flex-row align-items-center" style="position: absolute;top: 10px;right: 0px;">
                Welcome ${username}
                <button type="button" onClick="window.location='logout'" value="Logout"
                    class="btn ml-2 btn btn-secondary mx-2">
                    Logout
                </button>
            </div>
        </div>
        
            <div class="p-2 fs-3 text-center" style="position: relative;">

                Book Listing
           </div>
            <div class="d-flex flex-row align-items-center" style="position: absolute;top: 70px;right: 135px;">
                <button type="button" onClick="window.location='add-book'" value="Logout"
                    class="btn ml-2 btn btn-secondary mx-2">
                    Add a book
                </button>
            </div>
        <div class="container">
        
	<table class="table table-bordered ">
                    <thead>
                        <tr>
                        	<th class="text-center" scope="col">Book Code</th>
                            <th class="text-center" scope="col">Book Name</th>
                            <th class="text-center" scope="col">Author</th>
                            <th class="text-center" scope="col">Date Added</th>
                            <th class="text-center" scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        <c:forEach items="${bookList}" var="book" varStatus="loop">
                            <tr>
                                <td class="text-center align-middle"
                                    style="word-wrap: break-word;min-width: 160px;max-width: 160px;">
                                    <c:out value="${book.getBookCode()}" />
                                </td>
                                <td class="text-center align-middle">
                                    <c:out value="${book.getBookName()}" />
                                </td>
                                <td class="text-center align-middle">
                                    <c:out value="${book.getAuthor().getName()}" />
                                </td>
                                <td class="text-center align-middle">
                                    <c:out value="${book.getDateInformat()}" />
                                </td>
                                <td class="text-center justify-content-around align-middle">
                                    <button onClick="window.location='delete?id=${book.getBookCode()}'"
                                        class="d-flex flex-row btn btn-outline-primary" type="button"
                                        style="margin: auto;margin-bottom:5px;">
                                        <span class="material-icons">delete</span>Delete
                                    </button>
                                    <button
                                        onClick="window.location='edit-book?id=${book.getBookCode()}'"
                                        class="d-flex flex-row btn btn-outline-primary" type="button"
                                        style="margin: auto;">
                                        <span class="material-icons">edit</span>Edit
                                    </button>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                </div>
</body>
</html>