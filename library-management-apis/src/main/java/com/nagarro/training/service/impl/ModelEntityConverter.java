package com.nagarro.training.service.impl;

import org.springframework.stereotype.Component;

import com.nagarro.training.entity.AuthorEntity;
import com.nagarro.training.entity.BookEntity;
import com.nagarro.training.model.AuthorModel;
import com.nagarro.training.model.BookModel;
import com.nagarro.training.service.ModelEntityConverterInterface;

@Component
public class ModelEntityConverter implements ModelEntityConverterInterface {

	@Override
	public AuthorModel convertAuthorEntityToAuthorModel(AuthorEntity authorEntity) {
		AuthorModel authorModel = new AuthorModel(authorEntity.getAuthorId(), authorEntity.getName());
		return authorModel;
	}

	@Override
	public AuthorEntity convertAuthorModelToAuthorEntity(AuthorModel authorModel) {
		AuthorEntity authorEntity = new AuthorEntity();
		authorEntity.setAuthorId(authorModel.getAuthorId());
		authorEntity.setName(authorModel.getName());
		return authorEntity;
	}

	@Override
	public BookModel convertBookEntityToBookModel(BookEntity bookEntity) {
		BookModel bookModel = new BookModel(bookEntity.getBookCode(), bookEntity.getBookName(),this.convertAuthorEntityToAuthorModel(bookEntity.getAuthor()),bookEntity.getDate());
		return bookModel;
	}

	@Override
	public BookEntity convertBookModelToBookEntity(BookModel bookModel) {
		System.out.println(bookModel.toString());
		BookEntity bookEntity = new BookEntity();
		bookEntity.setBookCode(bookModel.getBookCode());
		bookEntity.setBookName(bookModel.getBookName());
		bookEntity.setAuthor(this.convertAuthorModelToAuthorEntity(bookModel.getAuthor()));
		System.out.println(this.convertAuthorModelToAuthorEntity(bookModel.getAuthor()).toString());
		return bookEntity;
	}

}
