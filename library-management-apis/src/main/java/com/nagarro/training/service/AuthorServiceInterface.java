package com.nagarro.training.service;

import org.springframework.http.ResponseEntity;

import com.nagarro.training.model.AuthorModel;

public interface AuthorServiceInterface {

	public ResponseEntity<Object> getAllAuthorModel();

	public ResponseEntity<Object> getAuthorById(int id);

	public ResponseEntity<Object> addAuthor(AuthorModel authorModel);

	public ResponseEntity<Object> updateAuthor(int id, AuthorModel authorModel);

	public ResponseEntity<Object> deleteAuthor(int id);

}
