package com.nagarro.training.service;

import com.nagarro.training.entity.AuthorEntity;
import com.nagarro.training.entity.BookEntity;
import com.nagarro.training.model.AuthorModel;
import com.nagarro.training.model.BookModel;

public interface ModelEntityConverterInterface {

	public AuthorModel convertAuthorEntityToAuthorModel(AuthorEntity authorEntity);

	public AuthorEntity convertAuthorModelToAuthorEntity(AuthorModel authorModel);

	public BookModel convertBookEntityToBookModel(BookEntity bookEntity);

	public BookEntity convertBookModelToBookEntity(BookModel bookModel);
}
