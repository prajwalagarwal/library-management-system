package com.nagarro.training.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.nagarro.training.entity.AuthorEntity;
import com.nagarro.training.model.AuthorModel;
import com.nagarro.training.repository.AuthorDao;
import com.nagarro.training.service.AuthorServiceInterface;
import com.nagarro.training.service.ModelEntityConverterInterface;

@Service
public class AuthorService implements AuthorServiceInterface {

	@Autowired
	AuthorDao authorDao;
	@Autowired
	ModelEntityConverterInterface modelEntityConverter;

	@Override
	public ResponseEntity<Object> getAllAuthorModel() {
		try {
			List<AuthorEntity> authorEntityList = new ArrayList<AuthorEntity>();
			List<AuthorModel> authorModelList = new ArrayList<AuthorModel>();
			authorDao.findAll().forEach(authorEntityList::add);
			for (AuthorEntity authorEntity : authorEntityList) {
				authorModelList.add(modelEntityConverter.convertAuthorEntityToAuthorModel(authorEntity));
			}
			if (authorModelList.isEmpty()) {
				return ResponseHandler.generateResponse("success", HttpStatus.NO_CONTENT, null);
			}
			return ResponseHandler.generateResponse("success", HttpStatus.OK, authorModelList);
		} catch (Exception e) {
			return ResponseHandler.generateResponse("error", HttpStatus.INTERNAL_SERVER_ERROR, null);
		}
	}

	@Override
	public ResponseEntity<Object> getAuthorById(int id) {
		Optional<AuthorEntity> authorEntity = authorDao.findById(id);
		if (authorEntity.isPresent()) {
			AuthorModel authorModel = modelEntityConverter.convertAuthorEntityToAuthorModel(authorEntity.get());
			return ResponseHandler.generateResponse("success", HttpStatus.OK, authorModel);
		} else {
			return ResponseHandler.generateResponse("No author present with this id", HttpStatus.NOT_FOUND, null);
		}
	}

	@Override
	public ResponseEntity<Object> addAuthor(AuthorModel authorModel) {
		try {
			AuthorEntity authorEntity = modelEntityConverter.convertAuthorModelToAuthorEntity(authorModel);

			AuthorEntity returnedAuthorEntity = authorDao.save(authorEntity);
			return ResponseHandler.generateResponse("success", HttpStatus.OK,
					modelEntityConverter.convertAuthorEntityToAuthorModel(returnedAuthorEntity));
		} catch (Exception e) {
			return ResponseHandler.generateResponse("error", HttpStatus.INTERNAL_SERVER_ERROR, null);
		}
	}

	@Override
	public ResponseEntity<Object> updateAuthor(int id, AuthorModel authorModel) {
		AuthorEntity authorEntity = authorDao.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Not found Author with id = " + id));
		authorEntity.setName(authorModel.getName());
		AuthorEntity returnedAuthorEntity = authorDao.save(authorEntity);
		return ResponseHandler.generateResponse("success", HttpStatus.OK,
				modelEntityConverter.convertAuthorEntityToAuthorModel(returnedAuthorEntity));
	}

	@Override
	public ResponseEntity<Object> deleteAuthor(int id) {
		try {
			authorDao.deleteById(id);
		} catch (Exception e) {
			return ResponseHandler.generateResponse("No author present with this id", HttpStatus.NOT_FOUND, null);
		}

		return ResponseHandler.generateResponse("success", HttpStatus.OK, null);
	}
}
