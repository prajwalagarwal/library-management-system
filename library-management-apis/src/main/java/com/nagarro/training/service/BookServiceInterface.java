package com.nagarro.training.service;

import org.springframework.http.ResponseEntity;

import com.nagarro.training.model.BookModel;

public interface BookServiceInterface {

	public ResponseEntity<Object> getAllBook();

	public ResponseEntity<Object> getBookById(int id);

	public ResponseEntity<Object> addBook(BookModel bookModel);

	public ResponseEntity<Object> updateBook(int id, BookModel bookModel);

	public ResponseEntity<Object> deleteBook(int id);

}
