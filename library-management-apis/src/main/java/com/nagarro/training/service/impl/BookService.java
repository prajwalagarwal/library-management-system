package com.nagarro.training.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.nagarro.training.entity.BookEntity;
import com.nagarro.training.model.BookModel;
import com.nagarro.training.repository.BookDao;
import com.nagarro.training.service.BookServiceInterface;
import com.nagarro.training.service.ModelEntityConverterInterface;

@Service
public class BookService implements BookServiceInterface {

	@Autowired
	BookDao bookDao;
	@Autowired
	ModelEntityConverterInterface modelEntityConverter;

	@Override
	public ResponseEntity<Object> getAllBook() {
		try {
			List<BookEntity> bookEntityList = new ArrayList<BookEntity>();
			List<BookModel> bookModelList = new ArrayList<BookModel>();
			bookDao.findAll().forEach(bookEntityList::add);

			for (BookEntity bookEntity : bookEntityList) {
				bookModelList.add(modelEntityConverter.convertBookEntityToBookModel(bookEntity));
			}
			if (bookModelList.isEmpty()) {
				return ResponseHandler.generateResponse("success", HttpStatus.NO_CONTENT, null);
			}
			return ResponseHandler.generateResponse("success", HttpStatus.OK, bookModelList);
		} catch (Exception e) {
			return ResponseHandler.generateResponse("error", HttpStatus.INTERNAL_SERVER_ERROR, null);
		}
	}

	@Override
	public ResponseEntity<Object> getBookById(int id) {
		Optional<BookEntity> bookEntity = bookDao.findById(id);
		if (bookEntity.isPresent()) {
			BookModel bookModel = modelEntityConverter.convertBookEntityToBookModel(bookEntity.get());
			return ResponseHandler.generateResponse("success", HttpStatus.OK, bookModel);
		} else {
			return ResponseHandler.generateResponse("No Book present with this id", HttpStatus.NOT_FOUND, null);
		}
	}

	@Override
	public ResponseEntity<Object> addBook(BookModel bookModel) {
		try {
			BookEntity bookEntity = modelEntityConverter.convertBookModelToBookEntity(bookModel);
			BookEntity returnedBookEntity;
			if (bookDao.findById(bookEntity.getBookCode()).isEmpty()) {
				bookEntity.setDate(new Date());
				returnedBookEntity = bookDao.save(bookEntity);
			} else {
				return ResponseHandler.generateResponse("Book already present with this id",
						HttpStatus.INTERNAL_SERVER_ERROR, null);
			}
			return ResponseHandler.generateResponse("success", HttpStatus.OK,
					modelEntityConverter.convertBookEntityToBookModel(returnedBookEntity));

		} catch (Exception e) {
			return ResponseHandler.generateResponse("error", HttpStatus.INTERNAL_SERVER_ERROR, null);
		}
	}

	@Override
	public ResponseEntity<Object> updateBook(int id, BookModel bookModel) {
		BookEntity bookEntity = bookDao.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Not found Book with id = " + id));
		bookEntity.setBookName(bookModel.getBookName());
		bookEntity.setAuthor(modelEntityConverter.convertAuthorModelToAuthorEntity(bookModel.getAuthor()));
		BookEntity returnedBookEntity = bookDao.save(bookEntity);
		return ResponseHandler.generateResponse("success", HttpStatus.OK,
				modelEntityConverter.convertBookEntityToBookModel(returnedBookEntity));
	}

	@Override
	public ResponseEntity<Object> deleteBook(int id) {
		try {
			bookDao.deleteById(id);
		} catch (Exception e) {
			return ResponseHandler.generateResponse("No book present with this id", HttpStatus.NOT_FOUND, null);
		}
		return ResponseHandler.generateResponse("success", HttpStatus.OK, null);
	}
}
