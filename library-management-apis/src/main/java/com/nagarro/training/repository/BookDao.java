package com.nagarro.training.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nagarro.training.entity.BookEntity;

public interface BookDao extends JpaRepository<BookEntity, Integer> {
	

}
