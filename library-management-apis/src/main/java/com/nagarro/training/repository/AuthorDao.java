package com.nagarro.training.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.webmvc.RepositoryRestController;

import com.nagarro.training.entity.AuthorEntity;

@RepositoryRestController
public interface AuthorDao extends JpaRepository<AuthorEntity, Integer> {

}
