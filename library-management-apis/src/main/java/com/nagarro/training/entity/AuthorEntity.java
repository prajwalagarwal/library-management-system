package com.nagarro.training.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "author")
public class AuthorEntity {

	@Id
	@GeneratedValue
	@Column(name = "authorId")
	private int authorId;
	@Column(name = "name")
	private String name;
	@OneToMany(mappedBy = "author")
	private Set<BookEntity> books;

	public AuthorEntity() {
		super();
	}

	public AuthorEntity(int authorId, String name, Set<BookEntity> book) {
		super();
		this.authorId = authorId;
		this.name = name;
		this.books = book;
	}

	public int getAuthorId() {
		return authorId;
	}

	public void setAuthorId(int authorId) {
		this.authorId = authorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<BookEntity> getBook() {
		return books;
	}

	public void setBook(Set<BookEntity> book) {
		this.books = book;
	}

	@Override
	public String toString() {
		return "AuthorEntity [authorId=" + authorId + ", name=" + name + ", book=" + books + "]";
	}

}
