package com.nagarro.training.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "books")
public class BookEntity {
	@Id
	@Column(name = "bookcode")
	private int bookCode;
	@Column(name = "bookname", nullable = false)
	private String bookName;
	@ManyToOne()
	@JoinColumn(name = "authorId", nullable = false)
	private AuthorEntity author;
	@Temporal(TemporalType.DATE)
	private Date date;

	public BookEntity() {
		super();
	}

	public BookEntity(int bookCode, String bookName, AuthorEntity author) {
		super();
		this.bookCode = bookCode;
		this.bookName = bookName;
		this.author = author;
	}

	
	public int getBookCode() {
		return bookCode;
	}

	public void setBookCode(int bookCode) {
		this.bookCode = bookCode;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public AuthorEntity getAuthor() {
		return author;
	}

	public void setAuthor(AuthorEntity author) {
		this.author = author;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	

}
