package com.nagarro.training.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nagarro.training.model.AuthorModel;
import com.nagarro.training.service.AuthorServiceInterface;

@RestController
@RequestMapping("librarymanagement/v1/authors")
public class AuthorController {

	@Autowired
	AuthorServiceInterface authorService;

	@GetMapping("")
	public ResponseEntity<Object> getAllAuthorModel() {
		return authorService.getAllAuthorModel();
	}

	@GetMapping("/{id}")
	public ResponseEntity<Object> getAuthorById(@PathVariable(value = "id", required = true) int id) {
		return authorService.getAuthorById(id);
	}

	@PostMapping("")
	public ResponseEntity<Object> addAuthor(@RequestBody(required = true) AuthorModel authorModel) {
		return authorService.addAuthor(authorModel);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Object> updateAuthor(@PathVariable("id") int id,
			@RequestBody(required = true) AuthorModel authorModel) {
		return authorService.updateAuthor(id, authorModel);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Object> deleteAuthor(@PathVariable("id") int id) {
		return authorService.deleteAuthor(id);
	}

}
