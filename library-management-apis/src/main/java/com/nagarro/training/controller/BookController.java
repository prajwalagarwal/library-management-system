package com.nagarro.training.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nagarro.training.model.BookModel;
import com.nagarro.training.service.BookServiceInterface;

@RestController
@RequestMapping("librarymanagement/v1/books")
public class BookController {

	@Autowired
	BookServiceInterface bookService;

	@GetMapping("")
	public ResponseEntity<Object> getAllBook() {
		return bookService.getAllBook();
	}

	@GetMapping("/{id}")
	public ResponseEntity<Object> getBookById(@PathVariable(value = "id", required = true) int id) {
		return bookService.getBookById(id);
	}

	@PostMapping("")
	public ResponseEntity<Object> addBook(@RequestBody(required = true) BookModel bookModel) {
		return bookService.addBook(bookModel);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Object> updateBook(@PathVariable(value = "id") int id,
			@RequestBody(required = true) BookModel bookModel) {
		return bookService.updateBook(id, bookModel);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Object> deleteBook(@PathVariable(value = "id", required = true) int id) {
		return bookService.deleteBook(id);
	}
}
